## dcat: Meta-analysis with a covariate differentially categorized across studies

### Installation

    install.packages("devtools")
    library("devtools")
    install_bitbucket(repo="martinaryee/dcat", username="martinaryee")


### After installation, to view diarrhea incidence data in R

     packagePath <- system.file(package="dcat")

     studies <- read.delim(file.path(packagePath, "extdata/studies.txt"))
     head(studies)
     outcomes <- read.delim(file.path(packagePath, "extdata/outcomes.txt"))
     head(outcomes)

